//
//  MyViews.swift
//  MySnapKitExamples
//
//  Created by Mathias H. Hansen on 16/10/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

//-----------------
// MARK: UINavigationController
//-----------------

/// Dont want the viewcontroller to start under the navigationcontroller.
/// I want it below
/// See https://stackoverflow.com/questions/19143353/why-does-uiviewcontroller-extend-under-uinavigationbar-while-
public func mhh_dontStartUnderNavCtrl(this: UIViewController) {
	this.edgesForExtendedLayout = []
}


public func mhh_createWindowWithRootView(root: UIViewController, useNav: Bool) -> UIWindow? {
	let frame = UIScreen.main.bounds
	let window: UIWindow? = UIWindow(frame: frame)
	if let window = window {
		window.rootViewController = useNav == true ? UINavigationController(rootViewController: root) : root
		//    if useNav {
		//      let navCtrl = UINavigationController(rootViewController: root)
		//      navCtrl.navigationBar.backgroundColor = .yellow
		//      navCtrl.navigationBar.backgroundColor = UIColor(red: 255/255.0, green: 255/255, blue: 0/255.0, alpha: 1.0)
		//      navCtrl.navigationBar.tintColor = .yellow //  textcolor
		//      window.rootViewController = navCtrl
		//    } else {
		//      window.rootViewController = root
		//    }
		
		window.makeKeyAndVisible()
		//    window.backgroundColor = .yellow
		//    window.rootViewController!.view.backgroundColor = .yellow
	}
	return window!
}




// TODO: mhh. Move this extension to own file
public extension UIAlertController {
	
	func asStandard(
		title: String = "",
		message: String = "",
		preferredStyle: UIAlertControllerStyle = .alert
		) -> UIAlertController {
		let this = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
		return this
	}
	
	/// asBottom cannot be used with TextFields
	func asBottom(
		title: String = "",
		message: String = ""
		) -> UIAlertController {
		self.title = title
		self.message = message
		return self
	}
	
	func withAction(
		title: String = "OK",
		action: UIAlertAction? = nil,
		handler: ((UIAlertAction) -> Void)? = nil
	) -> UIAlertController {
		
		let alertAction = action ?? UIAlertAction(
				title: NSLocalizedString(title, comment: "Default action"),
				style: .default,
				handler: handler
			)

		addAction(alertAction)
		return self
	}
	
	func withTextField(placeholderText: String = "", isPassword: Bool = false) -> UIAlertController {
		addTextField { (textField) in
			textField.placeholder = placeholderText
			if isPassword {
				textField.isSecureTextEntry = true
			}
		}
		return self
	}
}


public func mhh_hello() -> String { return "Hello MhhViews" }


// MARK: Placement


/// // TODO: write how to use
public struct Mhh_placement {
	var y: Y
	var x: X
	
	public init(x: X, y: Y) {
		self.x = x
		self.y = y
	}
	
}


public enum X {
	case left
	case right
	case center
}


public enum Y {
	case top
	case bottom
}


// MARK: Errors


enum NilError: Error {
	case shouldNotBeNil(String)
}
