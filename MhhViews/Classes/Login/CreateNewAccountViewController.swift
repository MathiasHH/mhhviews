//
//  CreateNewAccountViewController.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 26/01/2018.
//
//  Copyright 2017-2018 Mathias Hedemann Hansen.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import UIKit
import MhhCommon


open class CreateNewAccountViewController: UIViewController, UITextFieldDelegate {
	
	
	let email = InputTextField().asStandard(phText:"Email ").withRoundCorner()
	let password = InputTextField().asStandard(phText:"Password ").withRoundCorner().asPassword()
	let confirmPassword = InputTextField().asStandard(phText:"Confirm Password ").withRoundCorner().asPassword()
	var createAccountModel: AuthModel?
	
	
	public init(model: AuthModel) {
		//		super.init()
		print("login a")
		createAccountModel = model
		super.init(nibName: nil, bundle: nil)
	}
	
	// This extends the superclass.
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		print("login b")
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	// This is also necessary when extending the superclass.
	required public init?(coder aDecoder: NSCoder) {
		print("login c")
		fatalError("init(coder:) has not been implemented")
	}
	
	override open func viewDidLoad() {
		super.viewDidLoad()
		let superview = view!
		let padding = 10
		email.delegate = self
		superview.addSubview(email)
		
		password.delegate = self
		superview.addSubview(password)
		
		confirmPassword.delegate = self
		superview.addSubview(confirmPassword)
		
		let registerBtn = UIButton().asBtnRoundCorners(title: "Register").onClick(self, action: #selector(registerBtnAction))
		superview.addSubview(registerBtn)
		
		let backBtn = UIButton().asBtnRoundCorners(title: "Back").onClick(self, action: #selector(backButtonAction))
		superview.addSubview(backBtn)
		
		// setup constraints
		email.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(superview).offset(padding + 100)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(superview.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		password.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(email.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(confirmPassword.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		confirmPassword.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(password.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(registerBtn.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		registerBtn.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(confirmPassword.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(backBtn.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		backBtn.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(registerBtn.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			// todo should set the bottom constraint to not getting the error
			make.height.equalTo(44)
		})
	}
	
	@objc func registerBtnAction() {
		print("We clicked register button")
		let emailText = email.text!
		print("email text is: \(emailText)")
		let passwordText = password.text!
		print("password text is: \(passwordText)")
		let confirmPasswordText = confirmPassword.text!
		print("confirm password text is: \(confirmPasswordText)")
		
		createAccountModel?.tryToRegisterUser(
			with:emailText,
			and: passwordText,
			and: confirmPasswordText,
			callback:
			{ (error: Error?, user: Any?) in
				if let error = error {
					print("VC my error \(error) not created")
				}
				if let user = user {
					print("VC my user \(user) created")
				}
		})
		
			
	}
	
	@objc func backButtonAction() {
		print("We clicked back button")
		self.dismiss(animated: true)
	}
	
	@objc func injected() {
		print("I've been injected: \(self)")
		
	}
	
}



