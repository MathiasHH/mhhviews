//
//  ThemeOnceViewController.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 04/01/2018.
//

import UIKit

public class ThemeOnceViewController: ThemeViewController {
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		
		textLabel.text = "Click a button to choose a theme \nThe Theme can be changed later"
		//      textLabel.lineBreakMode = .byWordWrapping
		textLabel.numberOfLines = 0
		//      textLabel.snp.makeConstraints({(make) -> Void in
		//        make.height.equalTo(200)
		textLabel.textAlignment = .center
		//      })
		
		// add another action to buttons
		_ = darkThemeBtn.onClick(self, action: #selector(pickTheme))
		_ = lightThemeBtn.onClick(self, action: #selector(pickTheme))
	}
	
	
	@objc func pickTheme() {
		ThemeOnceModel().setAppHasRunForTheFirstTime()
		// Dismiss the view after one second
		let delayInSeconds = 1.0
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
			// H
			self.dismiss(animated: true)
		}
	}
	
}


struct ThemeOnceModel {
	func setAppHasRunForTheFirstTime() {
		var firstTimeAppStartsKeyValues = [String : String]()
		firstTimeAppStartsKeyValues[FirstTimeAppStarts.firstTimeKey] = "NO"
		saveDictionary(dict: &firstTimeAppStartsKeyValues, fileName: FirstTimeAppStarts.firstTimeAppStartsFileName)
	}
}
