//
//  UIButton+MhhStandard.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 23/11/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit



/// Extensions to UIButton
/// - author: Mathias H. Hansen.
/// - date: 11/10/2017
public extension UIButton {


  /// A custom UIButton (Android like Floating Action Button)
  /// - parameter title: A String. Default value is a +.
  /// - parameter colors: (fg: UIColor, bg: UIColor) or nil. If nil, background is UIColor.black, and foreground is UIColor.white.
  func asFAB(title: String = "+",
             colors: (fg: UIColor, bg: UIColor)? = nil) -> UIButton {
    layer.cornerRadius = 33

    // todo make border optional
//    let border = true

    setTitle(title, for: .normal)
    //the map closure is called with $0 as the unwrapped return value, from which a (fg: UIColor, bg: UIColor) is created. Otherwise map returns nil and the nil-coalescing operator evaluates to (nil, nil).
    let (fg, bg) = colors.map {
      ($0, $1)
    } ?? (nil, nil)
    setTitleColor(fg, for: .normal)
    backgroundColor = bg ?? .black
    layer.borderColor = bg == .black ? UIColor.lightGray.cgColor : UIColor.black.cgColor
    layer.borderWidth = 2

    return self
  }

  // TODO: write documentation  
  func asBtnRoundCorners(title: String = "UIButton",
                         colors: (fg: UIColor, bg: UIColor) = (fg: .black, bg: .white)) -> UIButton {
    backgroundColor = colors.bg
    setTitleColor(colors.fg, for: .normal)
    setTitle(title, for: .normal)
    layer.cornerRadius = 10
    clipsToBounds = true
    layer.borderColor = UIColor.black.cgColor
    layer.borderWidth = 1
    return self
  }


  /// Touch / Click
  /// - parameter vc: A UIViewController. The target object—that is, the object whose action method is called.
  /// - parameter action: The action to fire when the user clicks the button. The code is action: #selector(some_method). some_method must be prefixed with @objc.
  func onClick(_ vc: UIViewController,
               action: Selector) -> UIButton {
    addTarget(vc, action: action, for: .touchUpInside)
    return self
  }


  /// Touch / Click
  /// - parameter view: A UIView. The target object—that is, the object whose action method is called.
  /// - parameter action: The action to fire when the user clicks the button. The code is action: #selector(some_method). some_method must be prefixed with @objc.
  func onClick(_ view: UIView,
               action: Selector) -> UIButton {
    addTarget(view, action: action, for: .touchUpInside)
    return self
  }

  /// Add the button to the superview
  /// - author: Mathias H. Hansen
  /// - parameter superview: A UIView. The view that contains the button.
  /// - parameter inScreen: Upper left, lower left, lower right or upper right. See Mhh_placement or nil. If nill lower right corner is chosen.
  func placeView(superview: UIView,
                 inScreen: Mhh_placement? = nil,
                 with strategy: (_ this: UIButton,
                                 _  superview: UIView,
                                 _  inScreen: Mhh_placement?) -> Void) -> UIButton {
    superview.addSubview(self)
    strategy(self, superview, inScreen)

    return self
  }

  // TODO: write documentation
  /// Add two buttons to the superview. The button that calls this method is
  /// added to the left. The button that is a parameter is to the right.
  /// - author: Mathias H. Hansen
  /// - parameter superview: A UIView. The view that contains the button.
  /// - parameter under view // TODO:   
  /// - parameter other btn2 // TODO:   
  /// - parameter padding // TODO:
  /// - parameter strategy // TODO:
  func placeViewLeftToButton(superview: UIView,
                 under view: UIView? = nil,
                 other btn2: UIButton,
                 padding: CGFloat = 10,
                 with strategy: (_ this: UIButton,
                                 _ superview: UIView,
                                 _ view: UIView?,
                                 _ btn2: UIButton,
                                 _ padding: CGFloat) -> Void) -> UIButton {
    superview.addSubview(self)
    superview.addSubview(btn2)

    strategy(self, superview, view, btn2, padding)

    return self
 }

}