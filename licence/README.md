The Apache 2 license (given in full in LICENSE.txt) applies to all code in this repository which is copyright by Mathias Hedemann Hansen. The following sections of the repository contain third-party code, to which different licenses may apply:

To layout the code the the following third party code is used:


Path: MhhViews/Classes/MhhViewsSnapKitConstraints.swift, MhhViews/Classes/LayoutStrategies/SnapKitStrategy.swift 

    License: MIT (license/third_party/snapkit_license.txt)
    Origin: Copyright (c) 2011-Present SnapKit Team - https://github.com/SnapKit

