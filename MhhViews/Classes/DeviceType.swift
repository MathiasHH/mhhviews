//
//  RobotKPViewController.swift
//  MhhSnapKitExamples
//
//  Created by Mathias H. Hansen on 01/11/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


//https://stackoverflow.com/questions/46196964/detect-iphone-x-with-a-macro
//https://stackoverflow.com/questions/46192280/detect-if-the-device-is-iphone-x
public struct ScreenSize {
  static let width = UIScreen.main.bounds.size.width
  static let height = UIScreen.main.bounds.size.height
  static let maxLength = max(ScreenSize.width, ScreenSize.height)
  static let minLength = min(ScreenSize.width, ScreenSize.height)
  static let frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height)
}

public struct DeviceType {
  // iphone 4s is only supported to iOS 9.3.5
  public static let isIPhone4orLess
                                  = UIDevice.current.userInterfaceIdiom == .phone
      && ScreenSize.maxLength < 568.0
  public static let isIPhone5orSE = UIDevice.current.userInterfaceIdiom == .phone &&
      ScreenSize.maxLength == 568.0
  public static let isIPhone678   = UIDevice.current.userInterfaceIdiom == .phone &&
      ScreenSize.maxLength == 667.0
  public static let isIPhone678p  = UIDevice.current.userInterfaceIdiom == .phone &&
      ScreenSize.maxLength == 736.0
  public static let isIPhoneX     = UIDevice.current.userInterfaceIdiom == .phone &&
      ScreenSize.maxLength == 812.0

  public static let is_iPad
                                = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.maxLength == 1024.0
  public static let iS_iPad_Pro = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.maxLength == 1366.0

  public static func whichDevice<T>(values: [T]) -> T {

    if isIPhone5orSE {
      return values[0]
    } else if isIPhone678 {
      return values[1]
    } else if isIPhone678p {
      return values[2]
    } else if isIPhoneX {
      return values[3]
    } else {
      return -1 as! T
    }
  }
}