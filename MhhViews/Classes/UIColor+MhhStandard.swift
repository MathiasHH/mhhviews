//
//  UIColor+MhhStandard.swift
//  MhhViews
//  Created by Mathias Hedemann Hansen on 23/12/2017
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


import UIKit


/// https://stackoverflow.com/questions/36341358/how-to-convert-uicolor-to-string-and-string-to-uicolor-using-swift
public extension UIColor {


  convenience init(hexStr: String) {
    var cString: String = hexStr
        .trimmingCharacters(in: .whitespacesAndNewlines)
        .uppercased()

    if (cString.hasPrefix("#")) {
      cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
      self.init(cgColor: UIColor.gray.cgColor)
      return
    }

    var rgbValue: UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
    let green = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
    let blue = CGFloat(rgbValue & 0x0000FF) / 255.0
    let alpha = CGFloat(1.0)
    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }




//Convert RGBA String to UIColor object
//"rgbaString" must be separated by space "0.5 0.6 0.7 1.0" 50% of Red 60% of Green 70% of Blue Alpha 100%
//  public convenience init?(rgbaString : String) {
//    self.init(ciColor: CIColor(string: rgbaString))
//  }

//Convert UIColor to RGBA String
//  func toRGBAString()-> String {
//
//    var r: CGFloat = 0
//    var g: CGFloat = 0
//    var b: CGFloat = 0
//    var a: CGFloat = 0
//    self.getRed(&r, green: &g, blue: &b, alpha: &a)
//    return "\(r) \(g) \(b) \(a)"
//
//  }


// Convert UIColor to Hexadecimal String
  func toHexString() -> String {
    var r: CGFloat = 0
    var g: CGFloat = 0
    var b: CGFloat = 0
    var a: CGFloat = 0
    self.getRed(&r, green: &g, blue: &b, alpha: &a)
    return String(
        format: "%02X%02X%02X",
        Int(r * 0xff),
        Int(g * 0xff),
        Int(b * 0xff)
    )
  }


  static func hexStringToUIColor(hex: String) -> UIColor {
    var cString: String = hex
        .trimmingCharacters(in: .whitespacesAndNewlines)
        .uppercased()

    if (cString.hasPrefix("#")) {
      cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
      return UIColor.gray
    }

    var rgbValue: UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
  }

}


// MARK: Colors

public let myTheme = AppColor(hexStr2: "#2A2C37")
public let atomDark = AppColor(hexStr2: "#333743")
public let intellij = AppColor(hexStr2: "#2B2B2B")
public let white = AppColor(hexStr2: "#FFFFFF")
public let black = AppColor(hexStr2: "#000000")
public let blue = AppColor(hexStr2: "#0000FF")




