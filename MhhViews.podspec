#
# Be sure to run `pod lib lint MhhViews.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MhhViews'
  s.version          = '0.1.0'
  s.summary          = 'MhhViews is a view library.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'MhhViews is a view library. The purpose is to not write the same code all the time. It has two parts. One that extends UIKit and one that extends SnapKit.'

  s.homepage         = 'https://bitbucket.org/MathiasHH/mhhviews'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Mathias H. Hansen' => 'dev@mathiashh.dk' }
  s.source           = { :git => 'https://bitbucket.org/MathiasHH/MhhViews.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'MhhViews/Classes/**/*'
  
  # s.resource_bundles = {
  #   'MhhViews' => ['MhhViews/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'SnapKit', '~> 4.0.0'
  s.dependency 'MhhCommon' 
end
