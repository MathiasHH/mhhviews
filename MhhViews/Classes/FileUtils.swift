//
//  FileUtils.swift
// TODO: when creating the utils lib move to that lib  
//  Utils
//
//  Created by Mathias H. Hansen on 11/08/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


// MARK: Path


public func documentsDirectory() -> URL {
  let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
  return paths[0]
}


public func dataFilePath(str: String) -> URL {
  return documentsDirectory().appendingPathComponent(str)
}


// MARK: CRUD

func saveDictionary(
    dict: inout [String: String],
    fileName: String
) {
  let encoder = PropertyListEncoder()
  do {
    let data = try encoder.encode(dict)
    try data.write(to: dataFilePath(str: fileName),
                   options: Data.WritingOptions.atomic)
  } catch {
    print("Error encoding item!")
  }
}


public func loadDictionary(withFileName fileName: String) -> [String: String]? {
	let path = dataFilePath(str: fileName)
	if let data = try? Data(contentsOf: path) {
		let decoder = PropertyListDecoder()
		do {
			let dict: [String: String] = try decoder.decode(
				[String : String].self,
				from: data
			)
			return dict
		} catch {
			print("Error decoding item!")
		}
	}
	return nil
}


public func loadStr(forKey key: String, fileName: String) -> String? {
  let path = dataFilePath(str: fileName)
  if let data = try? Data(contentsOf: path) {
    let decoder = PropertyListDecoder()
    do {
      let dict: [String: String] = try decoder.decode(
          [String : String].self,
          from: data
      )
      return dict[key]
    } catch {
      print("Error decoding item!")
    }
  }
  return nil
}
