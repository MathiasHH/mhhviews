//
//  UITextField+MhhStandard.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 23/11/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit


//-----------------
// MARK: UITextField
//-----------------

/// A custom UITextField. It Subclasses UITextField
/// - author: Mathias H. Hansen
/// - copyright: Copyright (c) TODO use MIT or Apache 2 or similar
/// - date: 14/2/2017
/// - parameter this: A UIViewController.
/// - parameter inScreen: TODO Upper left, lower left, lower right or upper right. See Mhh_placement or nil. If nill lower right corner is chosen.
/// - parameter title: TODO A String or nil. If nil a + is the String.
/// - parameter colors: TODO (fg: UIColor, bg: UIColor) or nil. If nil, background is UIColor.black, and foreground is UIColor.white.
/// - parameter action: TODO The action to fire when the user clicks the button. The code is action: #selector(some_method). some_method must be prefixed with @objc.
/// - parameter placeholder TODO
public extension UITextField {
	
	
	func asStandard(phText: String? = nil,
                  adaptedPHTxt: Bool = false,
                  PHTextSizes: [CGFloat] = [10.5, 12.5, 14.5, 15]) -> UITextField {

    let placeholderColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1.0)
    let textSize: CGFloat = adaptedPHTxt ? DeviceType.whichDevice(values: PHTextSizes) : 17  // 17 is default size of placeholder text on UITextfield
			attributedPlaceholder = NSAttributedString(string: phText!, attributes: [NSAttributedStringKey.foregroundColor: placeholderColor, NSAttributedStringKey.font :UIFont(name: "HelveticaNeue-Medium", size: textSize)!])

//    self.font!.pointSize  // get size of textSize
		backgroundColor = UIColor.white
		tag = 1
		layer.borderColor = UIColor.black.cgColor
		layer.borderWidth = 1
		// we want the view to know when the user enters a grid size
		delegate = self as? UITextFieldDelegate
		autocorrectionType = .no
		return self
	}
	
	
	func withRoundCorner() -> UITextField {
		layer.cornerRadius = 10
		clipsToBounds = true
		return self
	}
	
	func asPassword() -> UITextField {
		self.isSecureTextEntry = true
		return self
	}
	
	
	func placeView(superview: UIView,
								 under view: UIView? = nil,
								 padding: CGFloat = 10,
								 with strategy: (_ this: UITextField,
                                 _ superview: UIView,
                                 _ view: UIView?,
                                 _ padding:CGFloat) -> Void) -> UITextField {
		superview.addSubview(self)
		strategy(self, superview, view, padding)
		return self
	}
	
	
}


public class InputTextField: UITextField {
	
	let inset: CGFloat = 10
	
	override init (frame : CGRect) {
		super.init(frame : frame)
	}
	
	
	required public init?(coder aDecoder: NSCoder) {
		fatalError("We create our views in code. We don't use storyboards or xip")
	}
	
	
	fileprivate func rect(_ bounds: CGRect) -> CGRect {
		return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 20, height: bounds.height)
	}
	
	
	override public func textRect(forBounds bounds: CGRect) -> CGRect {
		return rect(bounds)
	}
	
	
	override public func editingRect(forBounds bounds: CGRect) -> CGRect {
		return rect(bounds)
	}
	
	
	override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
		return rect(bounds)
	}
}
