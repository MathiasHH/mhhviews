//
//  LoginViewController.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 10/01/2018.
//
//  Copyright 2017-2018 Mathias Hedemann Hansen.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import UIKit
import MhhCommon


open class LoginViewController: UIViewController, UITextFieldDelegate {
	
	
	// TODO email input field shold be a email textfield not a standard one. So create a email input field in the lib
	let email = InputTextField().asStandard(phText:"Email").withRoundCorner()
	let password = InputTextField().asStandard(phText:"Password").withRoundCorner().asPassword()
	var loginModel: AuthModel?
	var gotoVC: UIViewController?
	
	
	// TODO create a factory to setup dependecies
	public init(model: AuthModel, gotoVC: UIViewController) {
		//		super.init()
		print("login a")
		loginModel = model
		self.gotoVC = gotoVC
		super.init(nibName: nil, bundle: nil)
	}
	
	// This extends the superclass.
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		print("login b")
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	// This is also necessary when extending the superclass.
	required public init?(coder aDecoder: NSCoder) {
		print("login c")
		fatalError("init(coder:) has not been implemented")
	}
	
	override open func viewDidLoad() {
		super.viewDidLoad()
		let superview = view!
		let padding = 10
		email.delegate = self
		superview.addSubview(email)
		
		password.delegate = self
		superview.addSubview(password)
		
		let loginBtn = UIButton().asBtnRoundCorners(title: "Log in").onClick(self, action: #selector(loginButtonTapped))
		superview.addSubview(loginBtn)
		
		let newAccountBtn = UIButton().asBtnRoundCorners(title: "Create New Account").onClick(self, action: #selector(newAccountButtonTapped))
		superview.addSubview(newAccountBtn)
		
		let resetPasswordBtn = UIButton()
			.asBtnRoundCorners(title: "Reset Password")
		  .onClick(self, action: #selector(resetPasswordButtonTapped))
		superview.addSubview(resetPasswordBtn)
		
		let resendVerificationEmailBtn = UIButton().asBtnRoundCorners(title: "Resend Verification Email").onClick(self, action: #selector(resendVerificationEmailButttonTapped))
		superview.addSubview(resendVerificationEmailBtn)
		
		// setup constraints
		email.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(superview).offset(padding + 100)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(superview.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		password.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(email.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(loginBtn.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		loginBtn.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(password.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(newAccountBtn.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		newAccountBtn.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(loginBtn.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(resetPasswordBtn.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		resetPasswordBtn.snp.makeConstraints({(make) -> Void in
			make.top.equalTo(newAccountBtn.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			make.bottom.equalTo(resendVerificationEmailBtn.snp.top).offset(-padding)
			make.height.equalTo(44)
		})
		
		resendVerificationEmailBtn.snp.makeConstraints({(make) -> Void in
		make.top.equalTo(resetPasswordBtn.snp.bottom).offset(padding)
			make.left.equalTo(superview).offset(padding)
			make.right.equalTo(superview).offset(-padding)
			// todo should set the bottom constraint to not getting the error
			make.height.equalTo(44)
		})
	}
	
	@objc func loginButtonTapped() {
		print("We clicked login button")
		let emailText = email.text!
		print("email text is: \(emailText)")
		let passwordText = password.text!
		print("password text is: \(passwordText)")
		
		loginModel?.login(
			with: emailText,
			and: passwordText,
			callback: { (error: Error?, user: Any?) in
				if let error = error {
					print("VC. Error \(error). User is not logged in")
				}
				if let user = user {
					print("VC. User \(user) is in")
					self.startFirstVC()
				}
		  }
		)
	}
	
	public func startFirstVC() {
		if let gotoVC = gotoVC {
			present(gotoVC, animated: true)
		}
	}
	
	@objc func newAccountButtonTapped() {
		print("We clicked  create account button")
		if let loginModel = loginModel {
		present(CreateNewAccountViewController(model: loginModel), animated: true)
		}
	}

	@objc func resetPasswordButtonTapped() {
		print("We clicked reset email button")
	  let resetAC = resetPasswordAlertCtrl()
		resetAC.addAction(resetAction(alertCtrl: resetAC))
		present(resetAC, animated: true)
	}

	func resetPasswordAlertCtrl() -> UIAlertController {
		return UIAlertController()
			.asStandard(title:"New Password", message: "Enter your email address and we'll send you a reset password link")
			.withAction(title: "Cancel")
			.withTextField(placeholderText: "Email")
	}
	
	func resetAction(alertCtrl: UIAlertController) -> UIAlertAction {
		return UIAlertAction(
			title: "Send",
			style: .default,
			handler: { alert -> Void in
				let email = alertCtrl.textFields![0] as UITextField
				print("email \(email.text!)")
				self.resetEmail(email: email.text!)
		})
	}

	func resetEmail(email: String) {
		loginModel?.resetEmail(email: email, callback: { (error: Error?) in
			if let error = error {
				let alert = UIAlertController()
					.asStandard(title: error.localizedDescription)
					.withAction()
				self.present(alert, animated: true)
			}
		})
	}
	
	@objc func resendVerificationEmailButttonTapped() {
		print("We clicked resend verification email button")
		let resendAC = resendVerificationEmailAlertCtrl()
		resendAC.addAction(resendAction(alertCtrl: resendAC))
		present(resendAC, animated: true)
	}
	
	func resendVerificationEmail(email: String, password: String) {
		loginModel?.sendVerificationEmail(
			email: email,
			password: password,
			callback: { (error: Error?, user: User?) in
				
				// TODO: mhh. Move this logic to model
				let text: String
				if let error = error
				{
					if let authError = error as? AuthError {
					  text = authError.simpleDescription
					} else {
						text = error.localizedDescription
					}
				}
				else {
					text = user!.verificationEmailSentAgainText
				}
				
				let alert = UIAlertController()
					.asStandard(title: text)
					.withAction()
				self.present(alert, animated: true)
		})
	}
	
	func resendVerificationEmailAlertCtrl() -> UIAlertController {
		return UIAlertController()
			.asStandard(
				title:"Resend Verification Email",
				message: "Enter your credentials and we send you a new verify your email link"
			)
			.withAction(title: "Cancel")
			.withTextField(placeholderText: "Email")
			.withTextField(placeholderText: "Password", isPassword: true)
	}
	
	func resendAction(alertCtrl: UIAlertController) -> UIAlertAction {
		return UIAlertAction(
			title: "Resend",
			style: .default,
			handler: { alert -> Void in
				let email = alertCtrl.textFields![0] as UITextField
				let password = alertCtrl.textFields![1] as UITextField
				password.isSecureTextEntry = true
				print("email \(email.text!) and password \(password.text!)")
				self.resendVerificationEmail(email: email.text!, password: password.text!)
		})
	}
	
	@objc func injected() {
		print("I've been injected: \(self)")
		
	}

	
}



