//
//  ViewController.swift
//  MySnapKitExamples
//
//  Created by Mathias H. Hansen on 14/10/2017.
//  Copyright © 2017 Mathias H. Hansen. All rights reserved.
//

import UIKit
import SnapKit
import MhhViews

class AViewController: UIViewController {

  var i = 0
  let box = UIView()
  let box2 = UIView()
  let textLabel = UILabel()

  override func viewDidLoad() {
    super.viewDidLoad()
    mhh_dontStartUnderNavCtrl(this: self)

    let superview = self.view!

    superview.addSubview(box)
    box.backgroundColor = UIColor.blue
    box.snp.makeConstraints { (make) -> Void in
      make.width.height.equalTo(50)
      make.center.equalTo(superview)
    }


    superview.addSubview(box2)
    box2.backgroundColor = UIColor.cyan
    box2.snp.makeConstraints { (make) -> Void in
      //			make.top.equalTo(superview).offset(20)
      //			make.left.equalTo(superview).offset(20)
      //			make.bottom.equalTo(superview).offset(-20)
      //			make.right.equalTo(superview).offset(-20)
      make.edges.equalTo(superview).inset(UIEdgeInsetsMake(40, 40, 40, 40))
    }

    superview.sendSubview(toBack: box2) // set box2 to the back of all the views
    //superview.bringSubview(toFront: box) // set box in the front of all views
    setupLabel(superview: superview)
  }

  private func setupLabel(superview: UIView) {
    textLabel.text = "View Controller A"
    textLabel.textColor = theme.appTextColor.color
    superview.addSubview(textLabel)
    textLabel.snp.makeConstraints({ (make) -> Void in
      make.height.equalTo(box)
      make.centerX.equalTo(superview)
      make.centerY.equalTo(superview).offset(50)
    })
  }


  @objc func injected() {
    viewDidLoad()
    i += 1
    let j = i % 2
    if j == 0 {
		// even
      box.backgroundColor = UIColor.yellow
      self.view.backgroundColor = UIColor.blue
    }
    else if j == 1 {
      box.backgroundColor = UIColor.blue
      self.view.backgroundColor = UIColor.yellow
    }

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
