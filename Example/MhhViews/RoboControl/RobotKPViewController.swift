//
//  RobotKPViewController.swift
//  MhhSnapKitExamples
//
//  Created by Mathias H. Hansen on 01/11/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import UIKit
import MhhViews

class RobotKPViewController: UIViewController {
//

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.brown
    rotate()

    let placement = Mhh_placement(x: X.left, y: Y.bottom)
//		let placement = Mhh_placement(x: X.right, y: Y.bottom)
//	let placement = Mhh_placement(x: X.right, y: Y.top)
//		let placement = Mhh_placement(x: X.left, y: Y.top)
//		let colors = (UIColor.white, UIColor.red)
    let colors = (UIColor.white, UIColor.purple)
    _ = UIButton().asFAB(title: "back", colors: colors)
                  .placeView(superview: self.view,
                             inScreen: placement,
                             with: usePlaceInCorner)
                  .onClick(self,
                           action: #selector(buttonClicked))
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    print("\nvc frame h: \(self.view.frame.height)")
    print("\nvc frame w: \(self.view.frame.width)\n")

    showAlertCtrl()
  }

  private func showAlertCtrl() {
  }

  override func loadView() {
    self.view = RobotKPView()
  }

  func rotate() {
    let value = UIInterfaceOrientation.landscapeLeft.rawValue
    UIDevice.current.setValue(value, forKey: "orientation")
  }


//	override var shouldAutorotate: Bool {
//		return true
//	}

  @objc func buttonClicked() {
//    print("Robot")
    dismiss(animated: true, completion: nil)
//    showAlertCtrl()
  }


  override var prefersStatusBarHidden: Bool {
    return true
  }


  @objc func injected() {
//		self.view.backgroundColor = .brown
//				self.view.backgroundColor = .orange
    print("I've been injected: \(self)")
//		viewDidLoad()
    loadView()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}
