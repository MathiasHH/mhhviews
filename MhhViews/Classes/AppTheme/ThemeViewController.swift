//
//  ThemeViewController.swift
//  MySnapKitExamples
//
//  Created by Mathias H. Hansen on 16/10/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import UIKit


open class ThemeViewController: UIViewController {


  let box = UIView()
  let textLabel = UILabel()
	// interesting read - https://www.bobthedeveloper.io/blog/swift-lazy-initialization-with-closures
	var darkThemeBtn =
		UIButton().asFAB(colors:(fg: white.color!, bg: myTheme.color!))
	var lightThemeBtn =
		UIButton().asFAB(colors:(fg: black.color!, bg: white.color!))

	
	override open func viewDidLoad() {
    super.viewDidLoad()
    setupView()
	
  }

  /// https://medium.com/cocoaacademymag/how-to-write-auto-layout-constraints-with-snapkit-in-ios-c5f95c7c695d
  func setupView() {
		mhh_dontStartUnderNavCtrl(this: self)
    let superview = self.view!
    setupBox(superview: superview)
    setupLabel(superview: superview)
    setupDarkThemeBtn()
    setupLightThemeBtn()
  }


  private func setupDarkThemeBtn() {
    darkThemeBtn = darkThemeBtn
			.onClick(self, action:#selector(buttonClicked))
			.placeView(superview: self.view,
								 inScreen: Mhh_placement(x: X.left, y: Y.bottom),
								 with: usePlaceInCorner)
  }


  private func setupLightThemeBtn() {
		lightThemeBtn = lightThemeBtn
			.onClick(self, action: #selector(buttonClicked(_:) ))
			.placeView(superview: self.view,
								with: usePlaceInCorner)
  }


  private func setupBox(superview: UIView) {
    superview.addSubview(box)
    box.backgroundColor = theme.appTextColor.color
    box.snp.makeConstraints { (make) -> Void in
      make.width.height.equalTo(50)
      make.center.equalTo(superview)
    }
  }


  private func setupLabel(superview: UIView) {
    textLabel.text = "Click a button to change theme"
    textLabel.textColor = theme.appTextColor.color
    superview.addSubview(textLabel)
    textLabel.snp.makeConstraints({ (make) -> Void in
      make.height.equalTo(box)
      make.centerX.equalTo(superview)
      make.centerY.equalTo(superview).offset(50)
    })
  }


  @objc func buttonClicked(_ sender:UIButton) {
    changeThisScreenColors(sender: sender)
    let backgroundColor = sender.backgroundColor!
    let appColorBg = AppColor(uiColor: backgroundColor)
    let foregroundColor = sender.currentTitleColor
    let appColorFg = AppColor(uiColor: foregroundColor)
    AppThemeModel.changeAppColors(colors: (bg: appColorBg, fg: appColorFg))
  }


  private func changeThisScreenColors(sender: UIButton) {
    view.backgroundColor = sender.backgroundColor
    box.backgroundColor = sender.currentTitleColor
    textLabel.textColor = sender.currentTitleColor
  }


  @objc func injected() {
    print("I've been injected: \(self)")

  }


}
