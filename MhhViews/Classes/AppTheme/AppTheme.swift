//
//  AppTheme.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 11/08/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import UIKit
import Foundation


// MARK: AppThemeModel

/// Global var so Views can easy get access to colors
public var theme = AppThemeModel()


/// Has the responsibility for the colors of the app
public struct AppThemeModel {

  let appColorsFileName = "AppColors.plist"
  var appColors = [String : String]()

  public let appBackgroundColorKey = "backgroundColor"
  private var _appBackgroundColor : AppColor?
  public var appBackgroundColor: AppColor {
    mutating get {
      if _appBackgroundColor == nil {
        // see if the user have a saved a app text color
        if let savedColor = loadStr(
            forKey: appBackgroundColorKey, fileName: appColorsFileName
        ) {
          _appBackgroundColor = AppColor(hexStr2: savedColor)
        }
        else {  // if no color set default app text color to black
          _appBackgroundColor = white
        }
      }
      return _appBackgroundColor!
    }
    set {
      _appBackgroundColor = newValue
      updateColors(value: newValue.hexStr!, forKey: appBackgroundColorKey)
      saveDictionary(
          dict: &appColors,
          fileName: appColorsFileName
      )
    }
  }

  public let appTextColorKey = "textColor"
  private var _appTextColor: AppColor? = nil
  public var appTextColor: AppColor {
    mutating get {
      if _appTextColor == nil {
        // see if the user have a saved a app text color
        if let savedColor = loadStr(
            forKey: appTextColorKey, fileName: appColorsFileName
        ) {
          _appTextColor = AppColor(hexStr2: savedColor)
        }
        else {  // if no color set default app text color to black
          _appTextColor = black
        }
      }
      return _appTextColor!
    }
    set {
      _appTextColor = newValue
      updateColors(value: newValue.hexStr!, forKey: appTextColorKey)
      saveDictionary(
          dict: &appColors,
          fileName: appColorsFileName
      )
    }
  }

  public let appButtonColor = blue


  mutating func updateColors(value: String, forKey key: String) {
    appColors[key] = value
  }


/// This can change the app background and text color.
/// - parameter window: The UIWindow. If not having the window it get fetches from the AppDelegate.
/// - parameter colors: A tuple containing two values. colors: (bg: UIColor, fg: UIColor).
  public static func changeAppColors(
      window: UIWindow? = nil,
      colors: (bg: AppColor, fg: AppColor)
//                                     = (bg: .white, fg: .black)
  ) {

    theme.appTextColor = colors.fg  // all text should use this global variable to get default theme
		theme.appBackgroundColor = colors.bg

    // change app background
    if let theWindow = window {
      theWindow.backgroundColor = colors.bg.color
    } else {
      let appDelegate = UIApplication.shared.delegate
      appDelegate!.window!?.backgroundColor = colors.bg.color
    }
  }


}


// MARK: App Color Model
// TODO: write purpose


public struct AppColor {


  private var _color: UIColor?
  public var color: UIColor? {
    get {
      return _color
    }
  }
  var hexStr: String? = nil


  init(hexStr2: String) {
    self.hexStr = hexStr2
    _color = UIColor.hexStringToUIColor(hex: hexStr2)
  }


  init(uiColor: UIColor) {
    _color = uiColor
    hexStr = uiColor.toHexString()
  }

  
}
