//
//  AppSetup.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 11/08/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

/// This method sets up app when we want to set app colors/theme when app starts
/// TODO: explain arguments
// TODO: test if it works. after the changes i have only testet the other setup method
public func setupApp(rootVC: UIViewController,
										 useNav: Bool = false,
										 colors: (bg: AppColor, fg: AppColor)) -> UIWindow {
	let window = mhh_createWindowWithRootView(root: rootVC, useNav: useNav)
	AppThemeModel.changeAppColors(window: window, colors: colors)
	
	return window!
}

/// This methods sets up app when the user controls the app color
/// black on white
/// TODO: explain arguments
public func setupApp(
	rootVC: UIViewController,
	useNav: Bool = false
	) -> UIWindow {
	let window = mhh_createWindowWithRootView(root: rootVC, useNav: useNav)
	window!.backgroundColor = theme.appBackgroundColor.color
	
	return window!
}


public struct FirstTimeAppStarts {
	
	
	static let firstTimeKey = "firstTime"
	static let firstTimeAppStartsFileName = "FirstTimeAppStarts.plist"

	
	public init() {
	}
	
	
	public func ifFirstTimeShowThemePicker(window: UIWindow) {
		let firstTime = loadStr(
			forKey: FirstTimeAppStarts.firstTimeKey, fileName: FirstTimeAppStarts.firstTimeAppStartsFileName
		)
		if firstTime != "NO" {
			print("It's the first time the app starts")
			let rootVC = window.rootViewController
			if let uiVC = rootVC {
				// todo take care of: Unbalanced calls to begin/end appearance transitions for <UINavigationController: 0x7f89a8021600>.
				uiVC.present(ThemeOnceViewController(), animated: false, completion: nil)
			}
		}
	}
	
	
}
