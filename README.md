# MhhViews

[![CI Status](http://img.shields.io/travis/Mathias H. Hansen/MhhViews.svg?style=flat)](https://travis-ci.org/Mathias H. Hansen/MhhViews)
[![Version](https://img.shields.io/cocoapods/v/MhhViews.svg?style=flat)](http://cocoapods.org/pods/MhhViews)
[![License](https://img.shields.io/cocoapods/l/MhhViews.svg?style=flat)](http://cocoapods.org/pods/MhhViews)
[![Platform](https://img.shields.io/cocoapods/p/MhhViews.svg?style=flat)](http://cocoapods.org/pods/MhhViews)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MhhViews is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MhhViews'
```

## Author

Mathias H. Hansen, dev@mathiashh.dk

## License

MhhViews is available under the The Apache 2 license. See the LICENSE file for more info.
