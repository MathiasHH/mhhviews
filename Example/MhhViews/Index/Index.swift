//
//  Index.swift
//  MySnapKitExamples
//
//  Created by Mathias H. Hansen on 14/10/2017.
//  Copyright © 2017 Mathias H. Hansen. All rights reserved.
//

import UIKit
import MhhViews

class Index: UITableViewController {


	var items = ["a", "b", "Robot Control", "Theme"]


	//	https://stackoverflow.com/questions/27103278/creating-a-uitableviewcell-programmatically-in-swift
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		let cell = IndexTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
		self.tableView.register(cell.classForCoder,
		                        forCellReuseIdentifier: "cell")
    tableView.backgroundColor = theme.appBackgroundColor.color
    tableView.reloadRows(at: tableView.indexPathsForVisibleRows!, with: .none)
	}


  override func viewDidLoad() {
    super.viewDidLoad()
  }


  override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! IndexTableViewCell
		let item: String = items[indexPath.row]
		// Configure the cell...
    cell.updateCell(for: cell, with: item)

		return cell
	}


// MARK: Navigation


	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		//		if let cell = tableView.cellForRow(at: indexPath) {
		let item = items[indexPath.row]
		//		}
		print(item)
		let navCtrl: UINavigationController = self.navigationController!
		switch item {
		case "a":
			navCtrl.pushViewController(AViewController(), animated: true)
		case "b":
			navCtrl.pushViewController(BViewController(), animated: true)
		case "Robot Control":
			present(RobotKPViewController(), animated: true, completion: nil)
//			navCtrl.pushViewController(RobotKPViewController(), animated: true)
    case "Theme":
      navCtrl.pushViewController(ThemeViewController(), animated: true)
		default:
			print("none picked")
		}

	}
	
	
}
