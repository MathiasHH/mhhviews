//
//  RobotKPView.swift
//  MhhViews_Example
//
//  Created by Mathias H. Hansen on 09/11/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import MhhViews


class RobotKPView: UIView, UITextFieldDelegate {


  @objc func injected() {
    print("I've been injected: \(self)")
    let padding = 10
    let superview = self

    setupLeftAndRightViewContainers(frame, padding, superview)

  }


  //	let roomSize, startPositionInput, navigationCommands : UITextView
  let inputViewContainer: UIView, outputViewContainer: UIView
  //	https://stackoverflow.com/questions/15978370/proper-practice-for-subclassing-uiview
//  let startPositionInputMessage, roomSizeMessage, navigationCommandsMessage, robotReport: UILabel
  let offGridOrOnGridSC = UISegmentedControl(items: ["På gitteret", "I cellen"])


  fileprivate func setupLeftAndRightViewContainers(_ frame: CGRect, _ padding: Int, _ superview: UIView) {

    setupLeftContainer(superview: superview, padding: padding)
    setupRightContainer(superview: superview, padding: padding)
  }

//-----------------
// MARK: Setup
//-----------------
  private func setupLeftContainer(superview: UIView, padding: Int) {
    inputViewContainer.backgroundColor = UIColor.yellow
    inputViewContainer.layer.borderColor = UIColor.black.cgColor
    inputViewContainer.layer.borderWidth = 2
    superview.addSubview(inputViewContainer)
    superview.addSubview(outputViewContainer)
    inputViewContainer.snp.makeConstraints { (make) -> Void in
      //			make.width.height.equalTo(50)
      //			make.center.equalTo(superview)
      if DeviceType.isIPhoneX {
        if #available(iOS 11, *) {
          make.top.equalTo(superview.safeAreaLayoutGuide.snp.top).offset(padding)
          make.left.equalTo(superview.safeAreaLayoutGuide.snp.left).offset(padding)
        }
      } else {
        make.top.equalTo(superview).offset(padding)
        make.left.equalTo(superview).offset(padding)
      }
      make.bottom.equalTo(superview).offset(-padding)
      make.right.equalTo(outputViewContainer.snp.left).offset(-padding)
      make.width.equalTo(outputViewContainer.snp.width)
    }


    // todo validate roomSize
    // TODO: delete label when we have view for errors
//    let roomSizeMessage: UILabel = {  // TODO: create method in library for a label and use it
//      let label = UILabel()
//      label.isHidden = true
//      label.text = "Forkert input"
//      label.font = label.font.withSize(11)
//      inputViewContainer.addSubview(label)
//      label.snp.makeConstraints(horizontalFillParentConstraint(in: inputViewContainer,
//                                                               under: roomSize,
//                                                               selfHeight: 10,
//                                                               horizontalPad: 14))
//      return label
//    }()

    // input field 1
    let roomSizeString = "Rum størrelse. Fx 5 7 (5 kolonner og 7 rækker)"
    let roomSize = InputTextField().asStandard(phText: roomSizeString, adaptedPHTxt: true)
    roomSize.delegate = self
    inputViewContainer.addSubview(roomSize)
    // input field 2
    let startPosPlaceholder = "Startposition. Fx 1 2 N"
    let startPos = InputTextField().asStandard(phText: startPosPlaceholder, adaptedPHTxt: true)
    inputViewContainer.addSubview(startPos)
    // input field 3
    let navComplaceholder = "Navigationskommandoer. Fx RFRFFRFRF"
    let navigationCommands = InputTextField().asStandard(phText: navComplaceholder, adaptedPHTxt: true)
    inputViewContainer.addSubview(navigationCommands)
    // reset button
    let resetBtn = UIButton().asBtnRoundCorners(title: "Forfra",
                                                colors: (fg: .white, bg: .red))
                             .onClick(self, action: #selector((reset)))
    inputViewContainer.addSubview(resetBtn)
    // report button
    let reportBtn = UIButton().asBtnRoundCorners(title: "Rapport",
                                                 colors: (fg:.white, bg: .blue))
                              .onClick(self, action: #selector((reset)))
    inputViewContainer.addSubview(reportBtn)
    // segmented control
//    let offGridOrOnGridSC = UISegmentedControl(items: ["På gitteret", "I cellen"])
    offGridOrOnGridSC.selectedSegmentIndex = 0
    inputViewContainer.addSubview(offGridOrOnGridSC)

    // setup constraints
    roomSize.snp.makeConstraints({(make) -> Void in
      make.top.equalTo(inputViewContainer).offset(padding)
      make.left.equalTo(inputViewContainer).offset(padding)
      make.right.equalTo(inputViewContainer).offset(-padding)
      make.bottom.equalTo(startPos.snp.top).offset(-padding)
      make.height.equalTo(startPos)
    })
    startPos.snp.makeConstraints({(make) -> Void in
      make.top.equalTo(roomSize.snp.bottom).offset(padding)
      make.left.equalTo(inputViewContainer).offset(padding)
      make.right.equalTo(inputViewContainer).offset(-padding)
      make.bottom.equalTo(navigationCommands.snp.top).offset(-padding)
      make.height.equalTo(roomSize)
    })
    navigationCommands.snp.makeConstraints({(make) -> Void in
      make.top.equalTo(startPos.snp.bottom).offset(padding)
      make.left.equalTo(inputViewContainer).offset(padding)
      make.right.equalTo(inputViewContainer).offset(-padding)
      make.bottom.equalTo(resetBtn.snp.top).offset(-padding)
      make.height.equalTo(roomSize)
    })
    resetBtn.snp.makeConstraints({(make) -> Void in
      make.top.equalTo(navigationCommands.snp.bottom).offset(padding)
      make.left.equalTo(inputViewContainer).offset(90)
      make.right.equalTo(reportBtn.snp.left).offset(-padding)
      make.bottom.equalTo(offGridOrOnGridSC.snp.top).offset(-padding)
      make.height.equalTo(roomSize)
      make.width.equalTo(reportBtn)
    })
    reportBtn.snp.makeConstraints({(make) -> Void in
      make.top.equalTo(navigationCommands.snp.bottom).offset(padding)
      make.left.equalTo(resetBtn.snp.right).offset(padding)
      make.right.equalTo(inputViewContainer).offset(-padding)
      make.bottom.equalTo(offGridOrOnGridSC.snp.top).offset(-padding)
      make.height.equalTo(roomSize)
      make.width.equalTo(resetBtn)
    })
    offGridOrOnGridSC.snp.makeConstraints({(make) -> Void in
      make.top.equalTo(reportBtn.snp.bottom).offset(padding)
      make.left.equalTo(inputViewContainer).offset(90)
      make.right.equalTo(inputViewContainer).offset(-padding)
      make.bottom.equalTo(inputViewContainer.snp.bottom).offset(-padding)
      make.height.equalTo(20)  // this is updated / calculated inupdateConstraints
    })

    // TODO: validate startPos
    // TODO: delete label when we have view for errors
//    let startPosMessage: UILabel = {  // TODO: create method in library for a label and use it
//      let label = UILabel()
//      label.isHidden = true
//      label.text = "Forkert input"
//      label.font = label.font.withSize(11)
//      inputViewContainer.addSubview(label)
//      label.snp.makeConstraints(horizontalFillParentConstraint(in: inputViewContainer,
//                                                               under: startPos,
//                                                               selfHeight: 10,
//                                                               horizontalPad: 14))
//      return label
//    }()

    // TODO: delete label when we have view for errors
    // Rapport
//    let robotRapport: UILabel = {
//      let label = UILabel()
//      label.text = "Change this text"
////      label.isHidden = true
//      label.font = label.font.withSize(20)
//      inputViewContainer.addSubview(label)
//      label.snp.makeConstraints(horizontalFillParentConstraint(in: inputViewContainer,
//                                                               under: navigationCommands,
////                                       selfHeight: 10,
//                                                               padding: 4,
//                                                               horizontalPad: 14))
//      return label
//    }()


    print("a self h frame: \(self.frame.height)")
  }

  // this is Apple's recommended place for updating constraints
// this method can get called multiple times in response to setNeedsUpdateConstraints
// which can be called by UIKit internally or in your code if you need to trigger an update to your constraints
  override func updateConstraints() {
    let height = self.frame.height
    print("a self h frame: \(height)")
    offGridOrOnGridSC.snp.updateConstraints({(make) -> Void in
      make.height.equalTo(height / 15)
    })

//    // according to Apple super should be called at end of method
    super.updateConstraints()
  }

  @objc func reset() {
    // TODO:  do reset
    print("reset was hit")
  }



  private func setupRightContainer(superview: UIView, padding: Int) {
    outputViewContainer.backgroundColor = UIColor.white
    outputViewContainer.layer.borderColor = UIColor.black.cgColor
    outputViewContainer.layer.borderWidth = 2
    outputViewContainer.snp.makeConstraints { (make) -> Void in
      make.top.equalTo(superview).offset(padding)
      make.left.equalTo(inputViewContainer.snp.right).offset(padding)
      make.bottom.equalTo(superview).offset(-padding)
      make.right.equalTo(superview).offset(-padding)
      make.width.equalTo(inputViewContainer.snp.width)

      make.height.equalTo(inputViewContainer)
    }
  }


  override init (frame : CGRect) {
    let padding = 10
    inputViewContainer = UIView()
    outputViewContainer = UIView()
    super.init(frame : frame)
    let superview = self
//    print("\nself frame h: \(self.frame.height)")
//    print("\nself frame w: \(self.frame.width)\n")
    setupLeftAndRightViewContainers(frame, padding, superview)
  }


  required init?(coder aDecoder: NSCoder) {
    fatalError("We create our views in code. We don't use storyboards or xip")
  }


//-----------------
// MARK: Setup UITextField delegate methods
//-----------------
  func textFieldDidBeginEditing(_ textField: UITextField) {

  }

  func textFieldDidEndEditing(_ textField: UITextField) {
//    let tag = textField.tag  // grid size
  }

  /*
  // Only override draw() if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func draw(_ rect: CGRect) {
  // Drawing code
  }
  */
}
