//
//  BViewController.swift
//  MySnapKitExamples
//
//  Created by Mathias H. Hansen on 16/10/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import UIKit
import MhhViews
//public extension UIButton {
//	func fab()  {
//		self.layer.cornerRadius = 33
//
//	}
//}
class BViewController: UIViewController {


  let textLabel = UILabel()


	override func viewDidLoad() {
		super.viewDidLoad()
//		self.view.backgroundColor = UIColor.orange
		
		// Do any additional setup after loading the view.
    let superview = self.view!
		setupView()
    setupLabel(superview)

	}
	
	/// https://medium.com/cocoaacademymag/how-to-write-auto-layout-constraints-with-snapkit-in-ios-c5f95c7c695d
	func setupView() {
		mhh_dontStartUnderNavCtrl(this: self)
		_ = UIButton().asFAB(colors:(fg:UIColor.red, bg: UIColor.white))
                  .onClick(self, action: #selector(buttonClicked))
                  .placeView(superview: self.view,
                             with: usePlaceInCorner)

	}
	
	@objc func buttonClicked() {
		print("BViewController")
	}


  private func setupLabel(_ superview: UIView) {
    textLabel.text = "View Controller B"
    textLabel.textColor = theme.appTextColor.color
    superview.addSubview(textLabel)
    textLabel.snp.makeConstraints({ (make) -> Void in
      make.height.equalTo(50)
//      make.centerX.equalTo(superview)
//      make.centerY.equalTo(superview).offset(50)
      make.center.equalTo(superview)
    })
  }

}
