//
//  MhhViewsSnapKitConstraints.swift
//  MhhViews
//
//  Created by Mathias H. Hansen on 01/11/2017.
//  Copyright 2017 Mathias Hedemann Hansen
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//
//  SnapKit
//
//  Copyright (c) 2011-Present SnapKit Team - https://github.com/SnapKit
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import UIKit
import SnapKit



//-----------------
// MARK: Place view with SnapKit
//-----------------


//-----------------
// MARK: Constraints
//-----------------

// TODO: documentation
public func placeInCornerConstraint(in superview: UIView,
                                    inScreen: Mhh_placement? = nil) -> (ConstraintMaker) -> Void {
  let placement = inScreen ?? Mhh_placement(x: X.right, y: Y.bottom)

  let (x, offSetX) = placement.x == X.left ? (superview.snp.left, 20) : (superview.snp.right, -20)
  let (y, offSetY) = placement.y == Y.top ? (superview.snp.top, 84) : (superview.snp.bottom, -20)
  return { (make) -> Void in
    make.width.equalTo(66)
    make.height.equalTo(66)
    make.bottom.equalTo(y).offset(offSetY)
    if placement.x == X.left {
      make.left.equalTo(x).offset(offSetX)
    } else {
      make.right.equalTo(x).offset(offSetX)
    }
  }
}


// TODO: documentation
public func horizontalFillParentConstraint(
    in superview: UIView,
    under view: UIView? = nil,
    selfHeight: CGFloat = 44,
    padding: CGFloat = 10,
    horizontalPad: CGFloat = 10) -> (ConstraintMaker) -> Void {
  return { (make) -> Void in
    if view == nil {
      make.top.equalTo(superview).offset(padding)
    } else {
      make.top.equalTo(view!.snp.bottom).offset(padding)
    }
    make.left.equalTo(superview).offset(horizontalPad)
    make.right.equalTo(superview).offset(-horizontalPad)
    make.height.equalTo(selfHeight)
  }
}

// TODO: documentation
public func horizontalToLeftOfBtnFillParentConstraint(
    in superview: UIView,
    under view: UIView? = nil,
    other btn2: UIButton,
    selfHeight: CGFloat = 44,
    padding: CGFloat = 10,
    horizontalPad: CGFloat = 10) -> (ConstraintMaker) -> Void {
  return { (make) -> Void in
    if view == nil {
      make.top.equalTo(superview).offset(padding)
    }else {
      make.top.equalTo(view!.snp.bottom).offset(padding)
    }
    make.left.equalTo(superview).offset(padding)
    make.right.equalTo(btn2.snp.left).offset(-padding)
    make.height.equalTo(44)
    make.width.equalTo(btn2)
  }
}


// TODO: documentation
public func horizontalToRightOfBtnFillParentConstraint(
    btn otherBtn: UIButton,
    in superview: UIView,
    under view: UIView? = nil,
    selfHeight: CGFloat = 44,
    padding: CGFloat = 10,
    horizontalPad: CGFloat = 10) -> (ConstraintMaker) -> Void {
  return { (make) -> Void in
    if view == nil {
      make.top.equalTo(superview).offset(padding)
    } else {
      make.top.equalTo(view!.snp.bottom).offset(padding)
    }
    make.left.equalTo(otherBtn.snp.right).offset(padding)
    make.right.equalTo(superview).offset(-padding)
    make.height.equalTo(44)
    make.width.equalTo(otherBtn)
  }
}
