//
//  DeviceTypeTests.swift
//  DeviceType_Tests
//
//  Created by Mathias H. Hansen on 19/12/2017.
//

import XCTest
@testable import MhhViews
//import MhhViews


//-----------------
// MARK: THESE TESTS OF DEVICE TYPES ARE MUTUALLY EXCLUSIVE BECAUSE THEY RUN ON DEVICE/SIMULATOR AND TEST WHICH DEVICE IT IS
//-----------------

class UIColorMhhStandardTests: XCTestCase {


//    override func setUp() {
//        super.setUp()
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//      super.tearDown()
//    }


//-----------------
// MARK: tests
//-----------------

//
  func test_UIColorExtension_has_hexString_when_using_hexStr_init() {
    // given
    let str = myTheme.hexStr!
    // when
    let co = AppColor(hexStr2: str)
    // then
    let hexStr = co.hexStr!
    XCTAssertEqual(hexStr, str, "text should be \(str)")


    let str2 = white.hexStr!
    // when
    let co2 = AppColor(hexStr2: str2)
    // then
    let hexStr2 = co2.hexStr!
    XCTAssertEqual(hexStr2, str2, "text should be \(str2)")

    XCTAssertNotEqual(co.hexStr!, hexStr2, "Strings should not equal")
  }


//  func test_UIColorExtension_dosnt_have_hexString_when_not_using_hexStr_init() {
//    // when
//    let co = AppColor()
//    // then
//    XCTAssertNil(co.hexStr2, "hexStr should return nil when not using the right init")
//  }


}

