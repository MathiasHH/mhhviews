//
//  DeviceTypeTests.swift
//  DeviceType_Tests
//
//  Created by Mathias H. Hansen on 19/12/2017.
//

import XCTest
@testable import MhhViews
//import MhhViews


//-----------------
// MARK: THESE TESTS OF DEVICE TYPES ARE MUTUALLY EXCLUSIVE BECAUSE THEY RUN ON DEVICE/SIMULATOR AND TEST WHICH DEVICE IT IS
//-----------------

class DeviceTypeTests: XCTestCase {


//    override func setUp() {
//        super.setUp()
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        super.tearDown()
//    }


//-----------------
// MARK: start of iphone 5, 5s and SE test. These test only make sense and should only be run on these devices/simulators
//-----------------

///*

  func test_DeviceType_whenIs_iPhone5or5SorSE_return_true_when_iPhone5or5SorSE() {
    XCTAssertTrue(DeviceType.isIPhone5orSE, "Should have been true to is iPhone 5/5s/SE)")
    XCTAssertFalse(DeviceType.isIPhone4orLess, "The device is a iPhone 5/5s/SE and not a iPhone 4/4s")
  }


  func test_DeviceType_whichDevice_whenIs_iPhone5or5SorSE_return_first_value() {
    // given
    let testValues = [12, 14, 20]

    // when
    let result = DeviceType.whichDevice(values: testValues)

    // then
    XCTAssertEqual(result, testValues.first!, "Should have been the value \(testValues.first!)")
    XCTAssertNotEqual(result, testValues[1], "Should not have been equal the value \(testValues[1])")
  }

//*/
}

