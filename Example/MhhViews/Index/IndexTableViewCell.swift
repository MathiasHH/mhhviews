//
//  TableViewCell.swift
//  MySnapKitExamples
//
//  Created by Mathias H. Hansen on 14/10/2017.
//  Copyright © 2017 Mathias H. Hansen. All rights reserved.
//

import UIKit
import MhhViews

class IndexTableViewCell: UITableViewCell {

	var label = UILabel()

	///------------
	//Method: Init with Style
	//Purpose:
	//Notes: This will NOT get called unless you call "registerClass, forCellReuseIdentifier" on your tableview
	///------------
	override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		
		label = UILabel(frame: CGRect(x: 119, y: 9, width: 216, height: 31))
    addSubview(label)
    setColors()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  public func updateCell(for cell: IndexTableViewCell,
                            with item: String) {
    configureText(for: cell, with: item)
    setColors()
  }


  private func setColors() {
    label.textColor = theme.appTextColor.color
    backgroundColor = theme.appBackgroundColor.color
  }


  private func configureText(for cell: IndexTableViewCell,
                     with item: String) {
    let label = cell.label
    label.text = item
  }
	

}
